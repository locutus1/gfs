# This Project is a fork of the [GFS Project - GNOME](https://github.com/slackport/gfs)
###### GNOME from Scratch/GNOME for Slackware

### Introduction

 The first question you should ask yourself is: 'if I don't like the GNOME, what am I doing here?'
 I'm talking about clarity, simplicity... I'm talking about tranquility and comfort... of course, everything have a price...
 And if you don't understand this, you're wasting your time reading me!
 This is GNOME 42... from Scratch (for Slackware)!

### Compatibilty
 
100% Compatible with slackware64-current, and slackware64-15.0

### Download and Install Compiled Packages
1. Download using this command:
**slackware64-current**
```bash
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-current/slackware64/gnome/42.2/ -c gfs-42.2-pkg64
```
1.1 Download using this command:
**slackware64-15.0**
```bash
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-15.0/slackware64/gnome/42.2/ -c gfs-42.2-pkg64
```

2. Before we start to install GNOME we're need to remove a couple of packages mozjs78 and xdg-desktop-portal-kde if KDE is installed on your system

As root, remove:
```
slackpkg remove mozjs78 xdg-desktop-portal-kde
```

3. As root, install:
```bash
upgradepkg --install-new --reinstall gfs-42.2-pkg64/*.txz
```

### Configuring
You need to add some groups and users to make things work better (I think), so:
1. In console (root), type:
```bash
groupadd -g 214 avahi
useradd -u 214 -g 214 -c "Avahi User" -d /dev/null -s /bin/false avahi
groupadd -g 303 colord
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord
groupadd -g 257 pcscd
useradd -u 257 -g pcscd -d /var/run/pcscd -s /bin/false pcscd
```
2. Avahi need to be run at boot, so edit your `/etc/rc.d/rc.local` adding these lines:
```bash
# Start avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
 /etc/rc.d/rc.avahidaemon start
fi
# Start avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd start
fi
# Start power-profiles-daemon
if [ -x /usr/libexec/power-profiles-daemon ]; then
  /usr/libexec/power-profiles-daemon &
fi
#Start pcscd daemon
if [ -x /etc/rc.d/rc.pcscd ]; then
  /etc/rc.d/rc.pcscd start
fi
#Start libvirt daemon
if [ -x /etc/rc.d/rc.libvirt ]; then
  /etc/rc.d/rc.libvirt start
fi
```
3. Also stop Avahi at shutdown, so edit your `/etc/rc.d/rc.local_shutdown` adding these lines:
```bash
# Stop avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd stop
fi
# Stop avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
  /etc/rc.d/rc.avahidaemon stop
fi
# Stop libvirt daemon
if [ -x /etc/rc.d/rc.libvirt ]; then
  /etc/rc.d/rc.libvirt stop
fi
```
4. Edit your /etc/inittab to go 4 runlevel ( id:3:initdefault: -> id:4:initdefault: )
5. Make sure gdm is the first one to run in the /etc/rc.d/rc.4 (remove the -nodaemon option)
8. Reboot your system

### How to compile and use GNOME 42 on Slackware -current
 1. In console (root), type:
```bash
git clone https://gitlab.com/slackernetuk/gfs.git -b master
cd gfs
sh gfs
```
 2. Edit your /etc/inittab to go 4 runlevel ( id:3:initdefault: -> id:4:initdefault: )
 3. Make sure gdm is the first one to run in the /etc/rc.d/rc.4 (remove the -nodaemon option)
 4. Reboot your system

 ### How to compile and use GNOME 42 on Slackware 15.0
 1. In console (root), type:
```bash
git clone https://gitlab.com/slackernetuk/gfs.git -b stable
cd gfs
sh gfs
```
 2. Edit your /etc/inittab to go 4 runlevel ( id:3:initdefault: -> id:4:initdefault: )
 3. Make sure gdm is the first one to run in the /etc/rc.d/rc.4 (remove the -nodaemon option)
 4. Reboot your system

 

### Slackware Goodies


**[Auto-CPUfreq](https://github.com/AdnanHodzic/auto-cpufreq)**


Automatic CPU speed & power optimizer for Linux based on active monitoring of laptop's battery state,CPU usage,<br> CPU temperature and system load. Ultimately allowing you to improve battery life without making any compromises.

If you would like to run Auto-CPUfreq at boot, so edit your /etc/rc.d/rc.local adding these lines:

```bash
# Start auto-cpufreq:
if [ -x /usr/share/auto-cpufreq/scripts/auto-cpufreq-init ]; then
  /usr/share/auto-cpufreq/scripts/rc.auto-cpufreq start 
fi
```

Also stop Auto-CPUfreq at boot, so edit your /etc/rc.d/rc.local adding these lines:

```bash
# Stop auto-cpufreq:
if [ -x /usr/share/auto-cpufreq/scripts/auto-cpufreq-init ]; then
  /usr/share/auto-cpufreq/scripts/rc.auto-cpufreq stop 
fi
```


### Thanks
- [Slackware UK](http://slackware.uk/)
- [Linux From Scratch](http://www.linuxfromscratch.org/)
- [Arch Linux Team](https://www.archlinux.org/)
- [Gentoo Linux Team](https://www.gentoo.org/)
- [SlackBuilds Team](https://slackbuilds.org/)
- [Raphael Fabeni](https://github.com/raphaelfabeni)
- [Willy Sudiarto Raharjo](https://github.com/willysr)
- [BobF](https://github.com/0xBOBF)
- [AdnanHodzic](https://github.com/AdnanHodzic)

### Contact.
 If you can help me to improve this project, please:
  - slackernetuk@gmail.com

I hope you enjoy it,

[Frank Honolka](https://www.facebook.com/frank.honolka.549/)

(2022, Great Britain)
